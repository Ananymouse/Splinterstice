# Splinterstice

Splinterstice is a decentralized web-based IM(instant messaging) platform that's client-side is a
fork of Kiwiirc, but with code copied from Revolt to combine aspects of those 2 other web clients, along with the addition of original features made from scratch, mainly the 3D UI, amongst other smaller features such as buttons and the likes. Splinterstice's target demographic are smaller niche online groups that hail from what (for lack of a better word) can best
be described as the "miscellaneous hidden services" of Tor and I2P, but those who're interested in such services can also join this decentralized messaging platform, so long as they know the name of it, for searching on either one of the aforementioned darknet services.